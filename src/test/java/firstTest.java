
import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class firstTest {
	private static First calc;
	
	@BeforeClass
	public static void start(){
		calc = new First();
	}
	
	@Test
	public void testAdd() {
		assertEquals(12, calc.add(9,3));
	}
	@Test
	public void testSub() {
		assertEquals(3, calc.sub(5,2));
	}
	@Test
	public void testMul() {
		assertEquals(18, calc.mul(6,3));
	}
	@Test
	public void testDiv() {
		assertEquals(4, calc.div(8,2));
	}
	@Test
	public void testDivByZero() {
		assertEquals(0, calc.div(3,0));
	}
	@Test
	public void testPresedence() {
		assertEquals(20, calc.add(calc.mul(5, 5), calc.div (10, 2)));
	}
	@Test
	public void test_Integration() {
		
		assertEquals(10, calc.sub(calc.mul(5,5), calc.div(10, 2)));
	}
}
